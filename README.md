My commit log:

commit f017fdecefc41ffe796ccfe9a1cb05772a7ad315
Author: Monique Dewanchand <mdewanchand>
Date:   Thu Oct 20 16:14:03 2022 +0200

    ViewLayer: Reduce object duplication syncing
    
    During object duplication the syncing is temporarily disabled.
    With {D15885} this isn't useful as when disabled the view_layer
    is still accessed to locate bases. This can be improved by first
    locating the source bases, then duplicate and sync and locate
    the new bases.
    
    This patch removes the resync forbid and improve the times
    that resyncing actually must happen.
    
    Reviewed By: mont29
    
    Maniphest Tasks: T73411
    
    Differential Revision: https://developer.blender.org/D15886

commit 4c320e2639ef41f093a71f6843b40ab64bdd021b
Author: Monique Dewanchand <mdewanchand>
Date:   Sun Oct 16 11:16:07 2022 +0200

    Fix T101394: Ensure all viewlayers on scene copy.
    
    When a scene copy is called, all viewlayers are ensured before they are
    copied. In case of multiple viewlayers, a viewlayer can be out of sync.
    This triggers an assert in the BKE_view_layer_copy_data.
    
    Reviewed By: mont29
    
    Maniphest Tasks: T101394
    
    Differential Revision: https://developer.blender.org/D16214

commit 68589a31ebfb79165f99a979357d237e5413e904
Author: Monique Dewanchand <mdewanchand>
Date:   Wed Sep 14 21:33:51 2022 +0200

    ViewLayer: Lazy sync of scene data.
    
    When a change happens which invalidates view layers the syncing will be postponed until the first usage.
    This will improve importing or adding many objects in a single operation/script.
    
    `BKE_view_layer_need_resync_tag` is used to tag the view layer to be out of sync. Before accessing
    `BKE_view_layer_active_base_get`, `BKE_view_layer_active_object_get`, `BKE_view_layer_active_collection`
    or `BKE_view_layer_object_bases` the caller should call `BKE_view_layer_synced_ensure`.
    
    Having two functions ensures that partial syncing could be added as smaller patches in the future. Tagging a
    view layer out of sync could be replaced with a partial sync. Eventually the number of full resyncs could be
    reduced. After all tagging has been replaced with partial syncs the ensure_sync could be phased out.
    
    This patch has been added to discuss the details and consequences of the current approach. For clarity
    the call to BKE_view_layer_ensure_sync is placed close to the getters.
    In the future this could be placed in more strategical places to reduce the number of calls or improve
    performance. Finding those strategical places isn't that clear. When multiple operations are grouped
    in a single script you might want to always check for resync.
    
    Some areas found that can be improved. This list isn't complete.
    These areas aren't addressed by this patch as these changes would be hard to detect to the reviewer.
    The idea is to add changes to these areas as a separate patch. It might be that the initial commit would reduce
    performance compared to master, but will be fixed by the additional patches.
    
    **Object duplication**
    During object duplication the syncing is temporarily disabled. With this patch this isn't useful as when disabled
    the view_layer is accessed to locate bases. This can be improved by first locating the source bases, then duplicate
    and sync and locate the new bases. Will be solved in a separate patch for clarity reasons ({D15886}).
    
    **Object add**
    `BKE_object_add` not only adds a new object, but also selects and activates the new base. This requires the
    view_layer to be resynced. Some callers reverse the selection and activation (See `get_new_constraint_target`).
    We should make the selection and activation optional. This would make it possible to add multiple objects
    without having to resync per object.
    
    **Postpone Activate Base**
    Setting the basact is done in many locations. They follow a rule as after an action find the base and set
    the basact. Finding the base could require a resync. The idea is to store in the view_layer the object which
    base will be set in the basact during the next sync, reducing the times resyncing needs to happen.
    
    Reviewed By: mont29
    
    Maniphest Tasks: T73411
    
    Differential Revision: https://developer.blender.org/D15885

commit 23276bcc37acc54f1e1814abdf482a432523c3a6
Author: Monique Dewanchand <mdewanchand>
Date:   Wed Sep 14 21:30:20 2022 +0200

    Adding `const Scene*` parameter in many areas.
    
    Related to {D15885} that requires scene parameter
    to be added in many places. To speed up the review process
    the adding of the scene parameter was added in a separate
    patch.
    
    Reviewed By: mont29
    
    Maniphest Tasks: T73411
    
    Differential Revision: https://developer.blender.org/D15930

commit 17501c146edc4af8a5e04565dc4d0b30ed5c5323
Author: Monique Dewanchand <mdewanchand>
Date:   Thu Sep 1 10:00:53 2022 +0200

    Cleanup: Remove/replace View Layer macros.
    
    This patch is a cleanup required before refactoring the view layer syncing
    process {T73411}.
    
    * Remove FIRSTBASE.
    * Remove LASTBASE.
    * Remove BASACT.
    * Remove OBEDIT_FROM_WORKSPACE.
    * Replace OBACT with BKE_view_layer_active_object.
    * Replace OBEDIT_FROM_VIEW_LAYER with BKE_view_layer_edit_object.
    
    Reviewed By: mont29
    
    Maniphest Tasks: T73411
    
    Differential Revision: https://developer.blender.org/D15799

commit e62a33e57256cb772e6dfce9ba7a3a58c3c08b0a
Author: Monique Dewanchand <mdewanchand>
Date:   Tue Jun 7 15:40:20 2022 +0200

    Nodes: Show node description in Node add menu
    
    Though no nodes have descriptions currently, this patch makes it
    possible to add descriptions that display in the add menu, for
    custom node systems and existing builtin nodes.
    
    Differential Revision: https://developer.blender.org/D14963

commit c751d40e070c6df907a526d7e3e17fc10611191e
Author: Monique Dewanchand <mdewanchand>
Date:   Mon Nov 30 07:55:30 2020 +0100

    Cleanup hardcoded render percentage to factor conversion
    
    During revision of {D8952} one of the comments was to make a function that converts the render percentage to a factor. This to avoid code duplication. However the duplicated code was already all over the compositor code. So in order to avoid this code duplication for {D8952} I propose to first cleanup the duplicated code and build patch {D8952} based on this clean up.
    
    The method that converts the render percentage to a factor is put in the CompositorContext. Why? The CompositorContext keeps DNA information like the renderdata. DNA, and thus the CompositorContext, keeps the size of the render resolution in percentage (user oriented). The compositor needs the size of the render resolution as a factor. So the CompositorContext seems like the obvious place to have this conversion method.
    
    Why not in de NodeBase? The method could've been added to the nodebase, but I wanted to keep the nodebase as clean as possible and not put simple "conversion" methods into this base class. Also I didn't really like the call flow: you'd always have to get the renderdata size from the context and then convert.
    Putting it in the CompositorContext avoids this extra invoke of a call.
    
    Why not in the Converter? See nodebase. And the Converter seems more like a class for "structural" and complex node tree conversions. Not the simple conversions.
    
    Reviewed By: Sergey Sharybin
    
    Differential Revision: https://developer.blender.org/D9566

commit d9b9a46aa3b5d5fcbc14f0aa3fa052f8e4c00c3f
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Fri Aug 31 15:00:38 2018 +0200

    Python: Enable MASS unit in FloatProperty
    
    MASS unit was already implemented for the C api. Only making sure it is
    accessible in the python api. Also added 'CAMERA' to the documentation as a valid option.

commit 49d1c8d8c689c917fa3fd7d309861f419ddac373
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Sat Apr 5 21:33:44 2014 +0200

    Fix issue for OPENCL problem on Macosx 10.9.2

commit 7142b970853f209f6c43319b7f862bcbdbea3728
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Sat Jan 18 18:20:21 2014 +0100

    Make it possible to hide/unhide a node during node transform operations.
    During drag the H key can be used to toggle the hide flag of the selected nodes.
    This makes it easier to 'attach' nodes to available links in narrow places.

commit 603289ffb228a9ed45a123cf6b7e752d1e184d01
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Sat Jun 8 19:56:11 2013 +0000

    Fix for bug [#35400] Dilate Erode Feather Bug - feathering wraps around image

commit 9cd01c6a5c68543ed40bfe33d5faa564006b0717
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Sat Feb 16 20:21:41 2013 +0000

    Added option for group node buffering in the compositor.
    
    Justa cluster did not have enough memory to handle all Mango 4k scenes.
    Option is default disabled and can be enabled in the performance panel.
    
     - At Mind -

commit 23bf087338204b45e5b72c1269bd013a88cf3f9d
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Wed Feb 6 08:40:12 2013 +0000

    Code clean up translate node
    added constants.
    moved the code to a separate class. so it can be reused for other nodes

commit ea723386c31e4c691e6a64f6c69a83b30f8c3010
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Mon Feb 4 17:38:37 2013 +0000

    updated fix for [#34089] Crash opening file containing translation node saved before rev54235
    some of our artists work to much on trunk :) Especially our beloved Pablo Vazquez.
    
    We increased the blender file sub-version for checking if translate nodes needed to be updated.
    
    Happy blending.

commit 2950214527812503b009995c638d2be37efaa22f
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Sun Feb 3 17:22:26 2013 +0000

    One fix for bug [#33785] compositor is (unnecessarily?) slow
    Added additional buffers - new subtree - for groupnodes.
    One needs to be aware of how groupnodes should be created.
    Having translate & scale nodes, with the translate inside the groupnode and the scale node outside, causes artefacts.
    Both should be inside or outside the groupnode. Same holds for other distort nodes.

commit a5cef69a0dc0b86d13d2fd16695db77094804c96
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Thu Jan 31 15:08:37 2013 +0000

     Apply patch [#33999] Wrapping mode for the "translate" compositing node
    
    this patch enables the translate node to wrap around the image borders. This is especially needed if the translate node is not used to position elements on a layer but when it is used instead for seamless backgrounds like mountains or clouds that should be repeated over time (by animating the x/y values).
    
    No trunk without docs! So here is my documentation: http://wiki.blender.org/index.php/User:Plasmasolutions/TranslateNodeExtension
    
    The code is properly documented and should be easy to read and understand. When there are any problems or issues, please comment, I'll tackle them right away!
    
    Greetings, Thomas Beck
    
    
     * optimized determination dependant areas
     * fixed some issues with scale node
    
    There are still some issues when scaling very small values (x=0.0001)
    
     - At Mind -

commit 4c971d572785c63a4c715210308664941c5c8f26
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Wed Jan 30 15:43:13 2013 +0000

    Patch by erwin94 [#34015] dilate/erode multithreading
    
    another patch for the dilate/erode step method, still without any functional changes.
    This time it keeps the general algorithm but uses the tile system to make it
    multithreaded. I could not measure a speedup on my 2-core laptop, but hope that
    it will be faster for more cores. The immediate speedup that is very visible though is
    that tiles come in as soon as they are calculated and a dilate/erode node does not
    block the whole image to be calculated.
    
    till then, David.

commit d4e5dc00f2facdd717ae99373ce7684e2c0d2a35
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Fri Jan 25 09:57:17 2013 +0000

    Increasing dilate erode number of steps.

commit 5d385b9a9a6f6981f849706397fd63d1511803ae
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Fri Jan 25 09:47:28 2013 +0000

    committed patch [#33972] dilate/erode optimization
    
    this patch optimizes the dilate/erode step method (hopefully without any functional change),
    making its speed not depend on the distance anymore.
    
    Couldn't detect funtional changes so committing. Haven't tested for speed gain.
    
     * credits to erwin94 David M

commit aa8fda324bfb8b224810441b4e98d625cbfda3ad
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Tue Jan 22 21:35:33 2013 +0000

    Displaying labelname with reroute nodes.
    It uses the default text color.

commit 315698543bfc675a0432b006a2fb94b4ed27b576
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Tue Jul 10 20:46:42 2012 +0000

    Fix for issue [#31981] for tiles opencl:
    initialize radius with correct value

commit e8e8ceaea2d8aa04d2216dbac82a0a907f8c435d
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Tue Jul 10 20:33:24 2012 +0000

    fix for Tiles bug - opencl:
    [#31981] Bokeh Blur Node - Size input socket does not accept input from Value Input node, Values smaller than 0.1 will produce black output

commit 8a4584d04daae39c76d1f46200a57d8f4f3c9d40
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Tue Jul 10 20:21:13 2012 +0000

    Fix for tiles bug:
    [#31981] Bokeh Blur Node - Size input socket does not accept input from Value Input node, Values smaller than 0.1 will produce black output

commit 82bad4bd6cb9f285ad541ccc0b91f16784f73c6b
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Wed Jun 20 20:05:21 2012 +0000

    Refactoring of tiles opencl implementation:
    - Moved methods from NodeOperation to OpenCLDevice
    - Added check on Nvidia for local size

commit 5fbeda7efd62e251dac2af881de9fe042f30a7a7
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Fri Jun 1 11:50:32 2012 +0000

    Optimize Gaussian blurs

commit 285a24b3e07b3441e98a0a502c42af50b9738f3a
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Fri Jun 1 10:20:24 2012 +0000

    Replaced tile based memory manager with a single aligned buffer
     - should increase speed with large node setups
     - enables caching of buffers in the node editor (in the future)
     - OpenCL part still needs some work

commit 13714e621d654f64a2b8c6f2772ffcb0ab101dd2
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Thu May 31 20:26:42 2012 +0000

    Fix for [#31662] Compositing: No Alpha if image/color connected to second input of math node

commit 722ce85ff6876ab85b7e934724e89fe24241f670
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Wed May 30 21:09:50 2012 +0000

    Fix for
    [#31562] New compositor crashes due to incorrect opencl initialization

commit b951c2be7f26caea266e3a27d9701b0374bdcad5
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Mon May 21 20:36:02 2012 +0000

    fix for
    [#31502] Defocus max radius help tekst

commit cd002016e907d4a42d4f26fbd36dd5536a6cd36a
Author: Monique Dewanchand <m.dewanchand@atmind.nl>
Date:   Mon May 21 19:58:23 2012 +0000

    Fix for
    [#31408] Code review testing: Button labels are invisible in many nodes
